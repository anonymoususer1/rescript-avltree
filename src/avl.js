// Generated by ReScript, PLEASE EDIT WITH CARE


function height(x) {
  if (x) {
    return x._3;
  } else {
    return 0;
  }
}

function create(l, v, r) {
  var hl = height(l);
  var hr = height(r);
  return /* Node */{
          _0: l,
          _1: v,
          _2: r,
          _3: hl >= hr ? hl + 1 | 0 : hr + 1 | 0
        };
}

function bal(l, v, r) {
  var hl = height(l);
  var hr = height(r);
  if (hl > (hr + 2 | 0)) {
    if (!l) {
      return /* Empty */0;
    }
    var lr = l._2;
    var lv = l._1;
    var ll = l._0;
    if (height(ll) >= height(lr)) {
      return create(ll, lv, create(lr, v, r));
    } else if (lr) {
      return create(create(ll, lv, lr._0), lr._1, create(lr._2, v, r));
    } else {
      return /* Empty */0;
    }
  }
  if (hr <= (hl + 2 | 0)) {
    return /* Node */{
            _0: l,
            _1: v,
            _2: r,
            _3: hl >= hr ? hl + 1 | 0 : hr + 1 | 0
          };
  }
  if (!r) {
    return /* Empty */0;
  }
  var rr = r._2;
  var rv = r._1;
  var rl = r._0;
  if (height(rr) >= height(rl)) {
    return create(create(l, v, rl), rv, rr);
  } else if (rl) {
    return create(create(l, v, rl._0), rl._1, create(rl._2, rv, rr));
  } else {
    return /* Empty */0;
  }
}

function compare_int(x, y) {
  if (x > y) {
    return 1;
  } else if (x === y) {
    return 0;
  } else {
    return -1;
  }
}

function add(x, tree) {
  if (!tree) {
    return /* Node */{
            _0: /* Empty */0,
            _1: x,
            _2: /* Empty */0,
            _3: 1
          };
  }
  var r = tree._2;
  var v = tree._1;
  var l = tree._0;
  var c = compare_int(x, v);
  if (c === 0) {
    return tree;
  } else if (c < 0) {
    return bal(add(x, l), v, r);
  } else {
    return bal(l, v, add(x, r));
  }
}

function min_elt(_def, _x) {
  while(true) {
    var x = _x;
    var def = _def;
    if (!x) {
      return def;
    }
    var l = x._0;
    if (!l) {
      return x._1;
    }
    _x = l;
    _def = x._1;
    continue ;
  };
}

function remove_min_elt(l, v, r) {
  if (l) {
    return bal(remove_min_elt(l._0, l._1, l._2), v, r);
  } else {
    return r;
  }
}

function internal_merge(l, r) {
  if (!l) {
    return r;
  }
  if (!r) {
    return l;
  }
  var rv = r._1;
  return bal(l, min_elt(rv, r), remove_min_elt(r._0, rv, r._2));
}

function remove(x, tree) {
  if (!tree) {
    return /* Empty */0;
  }
  var r = tree._2;
  var v = tree._1;
  var l = tree._0;
  var c = compare_int(x, v);
  if (c === 0) {
    return internal_merge(l, r);
  } else if (c < 0) {
    return bal(remove(x, l), v, r);
  } else {
    return bal(l, v, remove(x, r));
  }
}

function mem(x, _tree) {
  while(true) {
    var tree = _tree;
    if (!tree) {
      return false;
    }
    var c = compare_int(x, tree._1);
    if (c === 0) {
      return true;
    }
    _tree = c < 0 ? tree._0 : tree._2;
    continue ;
  };
}

var v = /* Empty */0;

for(var i = 0; i <= 100000; ++i){
  v = add(i, v);
}

for(var j = 0; j <= 100000; ++j){
  if (!mem(j, v)) {
    console.log("impossible");
  }
  
}

for(var k = 0; k <= 100000; ++k){
  v = remove(k, v);
}

var match = v;

if (match) {
  console.log("impossible");
} else {
  console.log("success");
}

export {
  height ,
  create ,
  bal ,
  compare_int ,
  add ,
  min_elt ,
  remove_min_elt ,
  internal_merge ,
  remove ,
  mem ,
  
}
/*  Not a pure module */
